import { Injectable } from '@angular/core';
import {Note} from "./note.model";

@Injectable({
  providedIn: 'root'
})
export class NotesService {
  notes: Note[] = [
    {
      title: "This is the first note",
      body: "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia"
    },
    {
      title: "This is the second note",
      body: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable"
    }
  ];
  constructor() { }

  getAll = () => {
    return this.notes;
  }

  get = (id: number) => {
    return this.notes[id];
  }

  getId = (note: Note) => {
    return this.notes.indexOf(note);
  }

  add = (note: Note) => {
    let newLength = this.notes.push(note);
    let index = newLength - 1;
    return index;
  }

  update = (id: number, title: string, body: string)  => {
    let note = this.notes[id];
    note.title = title;
    note.body = body;
  }

  delete = (id: number) => {
    this.notes.splice(id, 1);
  }
}
