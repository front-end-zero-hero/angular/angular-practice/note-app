import {Component, ElementRef, EventEmitter, Input, OnInit, Output, Renderer2, ViewChild} from '@angular/core';

@Component({
  selector: 'app-note-card',
  templateUrl: './note-card.component.html',
  styleUrls: ['./note-card.component.scss']
})
export class NoteCardComponent implements OnInit{
  @Input("title") title?: string;
  @Input("body") body?: string;
  @Input() link?: string;

  @ViewChild('truncator', {static: true}) truncator!: ElementRef<HTMLElement>;
  @ViewChild('bodyText', {static: true}) bodyText!: ElementRef<HTMLElement>;

  @Output('delete') deleteEvent: EventEmitter<void> = new EventEmitter<void>();
  constructor(private render: Renderer2) {
  }

  ngOnInit() {
    let viewAbleHeight = this.bodyText.nativeElement.offsetHeight;
    console.log(viewAbleHeight);
    if(this.bodyText.nativeElement.scrollHeight > viewAbleHeight) {
      this.render.setStyle(this.truncator.nativeElement, 'display', 'block');
    }
    else{
      this.render.setStyle(this.truncator.nativeElement, 'display', 'none');
    }
  }

  onXButtonClick() {
    this.deleteEvent.emit();
  }
}
