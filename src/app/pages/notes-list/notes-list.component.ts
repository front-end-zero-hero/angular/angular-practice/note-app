import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Note} from "../../shared/note.model";
import {NotesService} from "../../shared/notes.service";
import {animate, query, stagger, style, transition, trigger} from "@angular/animations";

@Component({
  selector: 'app-notes-list',
  templateUrl: './notes-list.component.html',
  styleUrls: ['./notes-list.component.scss'],
  animations: [
    trigger('itemAnim', [
      // ENTRY Animation
      transition('void => *', [
        // Initial State
        style({
          height: 0,
          opacity: 0,
          transform: 'scale(0.85)',
          'margin-bottom': 0,

          // We have to expand out the padding properties
          paddingTop: 0,
          paddingBottom: 0,
          paddingRight: 0,
          paddingLeft: 0
        }),
        // we first want to animate the spacing (which is includes height and margin)
        animate('50ms', style({
          height: '*',
          'margin-bottom':'*',
          paddingTop:'*',
          paddingBottom:'*',
          paddingLeft:'*',
          paddingRight:'*',
        })),
        animate(200)
      ]),
      transition('* => void', [
        // first scale up
        animate(50, style({
          transform: 'scale(1.05)'
        })),
        // then scale down back to normal size while beginning to fade out
        animate(50, style({
          transform: 'scale(1)',
          opacity: 0.75
        })),
        // scale down and fade out completely
        animate('120ms ease-out', style({
          transform: 'scale(0.68)',
          opacity: 0
        })),
        // then animate the spacing (which include height and margin)
        animate('150ms ease-out', style({
          opacity: 0,
          height: 0,
          paddingTop: 0,
          paddingBottom: 0,
          paddingRight: 0,
          paddingLeft: 0,
          'margin-bottom': '0'
        }))
      ])
    ]),
    trigger('listAnim', [
      transition('* => *', [
        query(':enter', [
          style({
            opacity: 0,
            height: 0
          }),
          stagger(100, [
            animate('0.2s ease')
          ])
        ], {
          optional: true
        })
      ])
    ])
  ]
})
export class NotesListComponent implements OnInit {

  notes: Note[] = [];
  filterNotes: Note[] = new Array<Note>();
  @ViewChild('filterInput') filterInputElementRef!: ElementRef<HTMLInputElement>;
  constructor(private noteService: NotesService) {
  }

  ngOnInit(): void {
    this.notes = this.noteService.getAll();
    this.filterNotes = this.noteService.getAll();
  }

  deleteNote = (note: Note) => {
    let noteId = this.noteService.getId(note);
    this.noteService.delete(noteId);
    this.filter(this.filterInputElementRef.nativeElement.value);
  }

  filter(query: string) {
    query = query.toLowerCase().trim();

    let allResults: Note[] = new Array<Note>();
    // spilit up the search query into individual words
    let terms: string[] = query.split(' ');
    // remove duplicate search terms
    terms = this.removeDuplicates(terms);
    // compile all relevant results into allResults arrays
    terms.forEach(term => {
      let result: Note[] = this.relevantNotes(term);
      allResults = [...allResults, ...result];
    });

    // allResults will include duplicate notes
    // because a particular note can be the result of many search terms
    // but we don't want to show the same note multiple times on the UI
    // so we first must remove the duplicates
    let uniqueResult = this.removeDuplicates(allResults);
    this.filterNotes = uniqueResult;

    this.sortByRelevancy(allResults);
  }

  removeDuplicates(arr: Array<any>): Array<any> {
    let uniqueResults: Set<any> = new Set<any>();
    // loop through the input array and add the item the set
    arr.forEach(e => uniqueResults.add(e));
    return Array.from(uniqueResults);
  }

  relevantNotes(query: string): Note[] {
    query = query.toLowerCase().trim();
    return this.notes.filter(note => {
      return !!(note.body?.toLowerCase().includes(query) || note.title?.toLowerCase().includes(query));
    });
  }

  sortByRelevancy(searchResult: Note[]) {
    // this method will calculate the relavency of a note based on the number of times it appears in
    // the search results
    let noteCountObj: any = {};
    searchResult.forEach(note => {
      let noteId = this.noteService.getId(note);
      if(noteCountObj[noteId]) {
        noteCountObj[noteId] += 1;
      }
      else {
        noteCountObj[noteId] = 1;
      }
    })

    this.filterNotes = this.filterNotes.sort((a: Note, b: Note) => {
      let aId = this.noteService.getId(a);
      let bId = this.noteService.getId(b);

      let aCount = noteCountObj[aId];
      let bCount = noteCountObj[bId];

      return bCount - aCount;
    })
  }

  generateNoteUrl(note: Note) {
    return this.noteService.getId(note).toString();
  }

  protected readonly HTMLInputElement = HTMLInputElement;
}
